console.log("Single linked list");
class Node{
    constructor(value){
        this.value=value;
        this.next=null;
    }
}
class SingleLinked{
    constructor(){
        this.head=null;
        this.size=0;
    }
    addNode(value){
        var _newnode=new Node(value);
        if(this.head===null)
            this.head=_newnode;
        else{
            let current=this.head;
            while(current.next){
                current=current.next;
            }
            current.next=_newnode;
        }
        this.size++;
    }
    removeNode(value){
        let current=this.head;
        if(current===null) console.log("Empty list");
        else{            
            let prev=null;
            while(current!=null){
                if(current.value===value){
                    if(prev===null)
                        this.head=current.next;
                        else
                    prev.next=current.next;
                    this.size--;
                    return -1;
                }
                else{
                    prev=current;
                     current=current.next;
                    
                }

            }
           
        }    
    }
    addSpecific(place,value){
        let _newnode=new Node(value);
        let current=this.head;
        let prev=null;
        if(current===null) console.log("Empty list");
        else{
            while(current!=null){
                if(current.value===place){
                    if(prev===null){
                    this.head=_newnode;
                    this.head.next=current;
                    }else{
                        prev.next=_newnode;
                        _newnode.next=current;
                    }
                    this.size++;
                    return -1;
                }else{
                    prev=current;
                    current=current.next;
                }
            }
        }

    }
}
var _sl=new SingleLinked();
_sl.addNode(10);
_sl.addNode(20);
_sl.addNode(30);
_sl.addNode(40);

console.log(_sl);
_sl.removeNode(30);
console.log(_sl);
_sl.addSpecific(20,70);
console.log(_sl);

console.log("********************Double linled list************************");
class DoubleNode{
    constructor(value){
        this.prev=null;
        this.value=value;
        this.next=null;
    }
}
class DoubleLinkedList{
    constructor(){
        this.head=null;
        this.size=0;
    }
    addEndNode(value){
        var _newnode=new DoubleNode(value);
        if(this.head===null)
            this.head=_newnode;
        else{
            let _current=this.head;
            while(_current.next){
                _current=_current.next;
            }
            _current.next=_newnode;
            _newnode.prev=_current;
        }
        this.size++;
        console.log(this.head);    
    }
}

var _dl=new DoubleLinkedList();
_dl.addEndNode(10);
_dl.addEndNode(20);